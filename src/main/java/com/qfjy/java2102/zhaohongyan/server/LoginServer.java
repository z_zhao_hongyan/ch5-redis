package com.qfjy.zhaohongyan2.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class LoginServer {
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;


    //    检验是否被限制
    public Map<String, Object> checkLogInfo(String username) {
        HashMap<String, Object> map = new HashMap<>();
        if (redisTemplate.hasKey(username)) {
            Long expire = redisTemplate.getExpire(username);
            if (expire > 0) {
                map.put("msg", true);
                return map;
            }else {
                map.put("msg", false);
            }
        }
        return map;
    }




//    登陆次数校验


    public  String getCount(String username,String password){
        String pwd = (String) redisTemplate.opsForValue().get(username);
        String msg = "";
//            判断库中密码与用户输入的是否一致
        if (!password.equals(pwd)) {
            redisTemplate.opsForValue().increment(username + "count");
            int count = (int) redisTemplate.opsForValue().get(username + "count");
            if (count >= 5) {
                redisTemplate.expire(username, 60, TimeUnit.MINUTES);
                return msg = "已被锁定1小时";
            } else {
//                      大于1小于5输入错误的提示；
                return msg = "一共5次输入机会，您已经输错" + count + "次";
            }
        }
        else {
            return msg="登陆成功";
        }

    }


}
