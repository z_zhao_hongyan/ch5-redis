package com.qfjy.zhaohongyan2.controller;


import com.qfjy.zhaohongyan2.server.LoginServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Map;
import java.util.concurrent.TimeUnit;
/**
 * 需求：
 * 用户在2分钟内，仅允许输入错误密码5次。
 * 如果超过次数，限制其登录1小时。（要求每登录失败时:输入错误，您还可以输入*次密码）
 */
//===================================================================

/**
 * 需求分析
 * ++判断是否被限制登录++
 * 1.没有被限制登录
 * 1.1 登录成功
 * 1.2 登录不成功--->记录登录失败的次数；-->判断key是否在Redis中存在
 * 1.2.1 存在，查一下失败的次数，如果小于5，提示可输入的次数；如果大于5 就限制登录1小时；
 * 1.2.3 不存在，提示还可输入4次；
 * ========================
 * 2. 被限制登录--->提示：请在*秒后登录！
 */

@Controller
@RequestMapping("zhy")
@Slf4j
public class LoginController {
    @Resource
    private LoginServer loginServer;

    //    跳转到登录页面
    @RequestMapping("ToLogin")
    public String ToLogin() {
        return "index.html";
    }
//    业务实现

    @GetMapping("Login")
    @ResponseBody
    public String login(String username, String password) {
        Map<String, Object> map = loginServer.checkLogInfo(username);
        if ((boolean)map.get("msg")){
            return "被限制了";
        }else {
           return  loginServer.getCount(username, password);

        }

    }

}
